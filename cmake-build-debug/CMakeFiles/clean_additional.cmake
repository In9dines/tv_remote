# Additional clean files
cmake_minimum_required(VERSION 3.16)

if("${CONFIG}" STREQUAL "" OR "${CONFIG}" STREQUAL "Debug")
  file(REMOVE_RECURSE
  "CMakeFiles\\tv_remote_autogen.dir\\AutogenUsed.txt"
  "CMakeFiles\\tv_remote_autogen.dir\\ParseCache.txt"
  "tv_remote_autogen"
  )
endif()
