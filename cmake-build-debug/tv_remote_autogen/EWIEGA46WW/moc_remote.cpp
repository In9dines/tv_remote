/****************************************************************************
** Meta object code from reading C++ file 'remote.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.15.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../../remote.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'remote.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.15.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Tv_remote_t {
    QByteArrayData data[16];
    char stringdata0[94];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Tv_remote_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Tv_remote_t qt_meta_stringdata_Tv_remote = {
    {
QT_MOC_LITERAL(0, 0, 9), // "Tv_remote"
QT_MOC_LITERAL(1, 10, 4), // "chn0"
QT_MOC_LITERAL(2, 15, 0), // ""
QT_MOC_LITERAL(3, 16, 4), // "chn1"
QT_MOC_LITERAL(4, 21, 4), // "chn2"
QT_MOC_LITERAL(5, 26, 4), // "chn3"
QT_MOC_LITERAL(6, 31, 4), // "chn4"
QT_MOC_LITERAL(7, 36, 4), // "chn5"
QT_MOC_LITERAL(8, 41, 4), // "chn6"
QT_MOC_LITERAL(9, 46, 4), // "chn7"
QT_MOC_LITERAL(10, 51, 4), // "chn8"
QT_MOC_LITERAL(11, 56, 4), // "chn9"
QT_MOC_LITERAL(12, 61, 7), // "nextChn"
QT_MOC_LITERAL(13, 69, 7), // "prevChn"
QT_MOC_LITERAL(14, 77, 7), // "plusVol"
QT_MOC_LITERAL(15, 85, 8) // "minusVol"

    },
    "Tv_remote\0chn0\0\0chn1\0chn2\0chn3\0chn4\0"
    "chn5\0chn6\0chn7\0chn8\0chn9\0nextChn\0"
    "prevChn\0plusVol\0minusVol"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Tv_remote[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      14,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   84,    2, 0x0a /* Public */,
       3,    0,   85,    2, 0x0a /* Public */,
       4,    0,   86,    2, 0x0a /* Public */,
       5,    0,   87,    2, 0x0a /* Public */,
       6,    0,   88,    2, 0x0a /* Public */,
       7,    0,   89,    2, 0x0a /* Public */,
       8,    0,   90,    2, 0x0a /* Public */,
       9,    0,   91,    2, 0x0a /* Public */,
      10,    0,   92,    2, 0x0a /* Public */,
      11,    0,   93,    2, 0x0a /* Public */,
      12,    0,   94,    2, 0x0a /* Public */,
      13,    0,   95,    2, 0x0a /* Public */,
      14,    0,   96,    2, 0x0a /* Public */,
      15,    0,   97,    2, 0x0a /* Public */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void Tv_remote::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<Tv_remote *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->chn0(); break;
        case 1: _t->chn1(); break;
        case 2: _t->chn2(); break;
        case 3: _t->chn3(); break;
        case 4: _t->chn4(); break;
        case 5: _t->chn5(); break;
        case 6: _t->chn6(); break;
        case 7: _t->chn7(); break;
        case 8: _t->chn8(); break;
        case 9: _t->chn9(); break;
        case 10: _t->nextChn(); break;
        case 11: _t->prevChn(); break;
        case 12: _t->plusVol(); break;
        case 13: _t->minusVol(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

QT_INIT_METAOBJECT const QMetaObject Tv_remote::staticMetaObject = { {
    QMetaObject::SuperData::link<QMainWindow::staticMetaObject>(),
    qt_meta_stringdata_Tv_remote.data,
    qt_meta_data_Tv_remote,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *Tv_remote::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Tv_remote::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Tv_remote.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int Tv_remote::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 14)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 14;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 14)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 14;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
