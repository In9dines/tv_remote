#include <QMainWindow>
#include <QLineEdit>
#include <QSlider>

class Tv_remote : public QMainWindow
{
    Q_OBJECT
public:
    Tv_remote(QWidget *parent = nullptr) : QMainWindow(parent) {}
    ~Tv_remote() {}

    QLineEdit *lineEdit_h = nullptr;
    QLineEdit *lineEdit_l = nullptr;
    QSlider *vertSlider = nullptr;
public slots:
    void chn0()
    {
        lineEdit_l->setText(QString::fromStdString(channels[0]));
        index = 0;
    }
    void chn1()
    {
        lineEdit_l->setText(QString::fromStdString(channels[1]));
        index = 1;
    }
    void chn2()
    {
        lineEdit_l->setText(QString::fromStdString(channels[2]));
        index = 2;
    }
    void chn3()
    {
        lineEdit_l->setText(QString::fromStdString(channels[3]));
        index = 3;
    }
    void chn4()
    {
        lineEdit_l->setText(QString::fromStdString(channels[4]));
        index = 4;
    }
    void chn5()
    {
        lineEdit_l->setText(QString::fromStdString(channels[5]));
        index = 5;
    }
    void chn6()
    {
        lineEdit_l->setText(QString::fromStdString(channels[6]));
        index = 6;
    }
    void chn7()
    {
        lineEdit_l->setText(QString::fromStdString(channels[7]));
        index = 7;
    }
    void chn8()
    {
        lineEdit_l->setText(QString::fromStdString(channels[8]));
        index = 8;
    }
    void chn9()
    {
        lineEdit_l->setText(QString::fromStdString(channels[9]));
        index = 9;
    }
    void nextChn()
    {
        if(index + 1 > 9)
        {
            index = 0;
        }
        lineEdit_l->setText(QString::fromStdString(channels[index + 1]));
    }
    void prevChn()
    {
        if(index - 1 < 0)
        {
            index = 9;
        }
        lineEdit_l->setText(QString::fromStdString(channels[index - 1]));
    }
    void plusVol()
    {
        if(vol + 10 > 100)
        {
            lineEdit_h->setText("100");
        }
        else
        {
            vertSlider->setSliderPosition(vol + 10);
            lineEdit_h->setText(QString::number(vol + 10));
        }
    }
    void minusVol()
    {
        if(vol - 10 < 100)
        {
            lineEdit_h->setText("0");
        }
        else
        {
            vertSlider->setSliderPosition(vol - 10);
            lineEdit_h->setText(QString::number(vol - 10));
        }
    }
private:
    std::string channels[10] {"STS", "TNT", "NTV", "1", "MuzTv", "Russia1", "TV3", "Discovery", "NatGeography", "Pyatnitsa"};
    int index = 0;
    int vol = 0;
};