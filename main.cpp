#include <QApplication>
#include <QPushButton>
#include "./ui_tv_remote_controller.h"
#include "remote.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Tv_remote window(nullptr);
    Ui::MainWindow remote;
    remote.setupUi(&window);
    remote.lineEdit = window.lineEdit_l;
    remote.lineEdit_2 = window.lineEdit_h;
    remote.verticalSlider = window.vertSlider;
    window.setFixedSize(240, 360);
    window.show();
    return QApplication::exec();
}